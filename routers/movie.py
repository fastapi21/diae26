from fastapi import Path, Query, Depends, APIRouter
from fastapi.responses import JSONResponse, HTMLResponse
from typing import List
from config.database import Session
# from middlewares.jwt_bearer import JWTBearer # Ya no es necesario importar esto
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()

@movie_router.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>Hello world</h1>')

@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200)
def get_all_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_all_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies/{id}', tags=['movies'], response_model=Movie)
def get_movie(id: int = Path(..., description="The ID of the movie to retrieve")) -> Movie:
    db = Session()
    result = MovieService(db).get_movie_by_id(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Movie not found"})
    return result

@movie_router.get('/movies/category/{category}', tags=['movies'], response_model=List[Movie])
def get_movies_by_category(category: str = Path(..., description="The category of the movies to retrieve")) -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies_by_category(category)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.post('/movies', tags=['movies'], response_model=dict, status_code=201)
def create_movie(movie: Movie) -> dict:
    db = Session()
    MovieService(db).create_movie(movie)
    return {"message": "Movie created"}

@movie_router.put('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
    db = Session()
    result = MovieService(db).update_movie(id, movie)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Movie not found"})
    return {"message": "Movie updated"}

@movie_router.delete('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def delete_movie(id: int) -> dict:
    db = Session()
    result = MovieService(db).delete_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Movie not found"})
    return {"message": "Movie deleted"}
